let gameDiv;

let gameW;
let gameH;

let gameWdAsPixels;
let gameHgAsPixels;

let pixelSize;

let gameProperties;

let inputChanged;
let keys = {};

const updateVariables = () => {
    ClientVars.gameDiv = document.getElementById("game");

    ClientVars.gameW = window.innerWidth;
    ClientVars.gameH = window.innerHeight;

    ClientVars.pixelSize = pixelCalculation();

    ClientVars.gameWdAsPixels = ClientVars.gameW / ClientVars.pixelSize;
    ClientVars.gameHgAsPixels = ClientVars.gameH / ClientVars.pixelSize;
}

const toClientPixelSize = (value) => {
    return value * ClientVars.pixelSize;
}

const reverseClientPixelSize = (value) => {
    return value / ClientVars.pixelSize;
}

const minPixelSize = 1;
const maxPixelSize = 3;

const pixelCalculation = () => {
    const hgPixelSize = Math.round((ClientVars.gameH - 270) * ((maxPixelSize - minPixelSize) / (540 - 270)) + minPixelSize);
    const wdPixelSize = Math.round((ClientVars.gameW - 480) * ((maxPixelSize - minPixelSize) / (960 - 480)) + minPixelSize);
    const pixelSize = hgPixelSize > wdPixelSize ? wdPixelSize : hgPixelSize;
    return pixelSize >= 1 ? pixelSize : 1;
};

export const ClientVars = {
    gameDiv,

    gameW,
    gameH,

    gameWdAsPixels,
    gameHgAsPixels,

    pixelSize,

    gameProperties,

    inputChanged,
    keys,

    updateVariables,

    toClientPixelSize,
    reverseClientPixelSize
}