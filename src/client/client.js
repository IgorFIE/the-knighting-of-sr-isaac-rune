import { GameState } from "../enums/game-state-type";
import { InputKey } from "../enums/movement-type";
import { CircleObject } from "../logic/collision-objects/circle-object";
import { playerSpeed } from "../logic/game-variables";
import { ClientVars } from "./client-variables";
import { GameDraw } from "./gameDraw/game-draw";
import { MainMenu } from "./mainMenu/main-menu";

let yourId, mainMenu, gameDraw, updatePlayerID, isGamePlaying;

const inputMsDelay = 100; // equal 10 inputs every second

const useInterpolators = false;
const useLatencyInterpolator = false;
const interpolators = {};

Rune.initClient({
  onChange: ({ game, previousGame, futureGame, yourPlayerId }) => {
    switch (game.gameState) {
      case GameState.MAIN_MENU:
        if (!mainMenu || !mainMenu.isDisplaying) {
          if (mainMenu) {
            mainMenu.reInit();
          } else {
            ClientVars.updateVariables();
            mainMenu = new MainMenu(document.getElementById("menu"), game.players[yourPlayerId].colors);
            yourId = yourPlayerId;
          }
        }
        break;

      case GameState.INIT_GAME: {
        if (mainMenu.isDisplaying) {
          mainMenu.hide();
          gameDraw = new GameDraw(ClientVars.gameProperties, game.players, yourPlayerId);
          setTimeout(() => {
            Rune.actions.drawCompleted();
            updatePlayerID = setInterval(updatePlayerInput, inputMsDelay);
          }, 0);
        }
        break;
      }

      case GameState.PLAYING:
        isGamePlaying = true;

        gameDraw.update(game);

        Object.keys(game.players).forEach(id => {
          if (useInterpolators) {
            if (!interpolators[id]) {
              if (useLatencyInterpolator) {
                interpolators[id] = Rune.interpolatorLatency({ maxSpeed: playerSpeed });
              } else {
                interpolators[id] = Rune.interpolator();
              }
            }
            interpolators[id].update({
              game: [game.players[id].collisionObj.x, game.players[id].collisionObj.y],
              futureGame: [futureGame.players[id].collisionObj.x, futureGame.players[id].collisionObj.y],
            });
          } else {
            gameDraw.players[id].updateDraw(game.players[id].collisionObj);
          }
        });
        break;
    }
  }
});

const updatePlayerInput = () => {
  if (ClientVars.inputChanged) {
    ClientVars.inputChanged = false;
    Rune.actions.updatePlayer({
      keys: { ...ClientVars.keys },
      atks: {
        [InputKey.LEFT_ATK]: { ...gameDraw.players[yourId].playerLeftWeapon.atkLine },
        [InputKey.RIGHT_ATK]: { ...gameDraw.players[yourId].playerRightWeapon.atkLine }
      }
    });
  }
}

if (useInterpolators) {
  const clientGameLoop = () => {
    if (isGamePlaying) {
      Object.keys(interpolators).forEach(id => {
        const newPos = interpolators[id].getPosition();
        const newCollision = new CircleObject(newPos[0], newPos[1], 0);
        gameDraw.players[id].updateMovement(newCollision);
      });
    }
    requestAnimationFrame(clientGameLoop);
  }
  clientGameLoop();
}
