import { ClientVars } from "../client-variables";

export const deadAnim = () => {
    return {
        rotate: [0 + "deg", 90 + "deg"],
        easing: ["ease-in-out", "ease-in-out"],
        offset: [0, 1]
    };
};

export const walk = () => {
    return {
        translate: [0, "0 " + ClientVars.toClientPixelSize(-3) + "px", 0],
        rotate: [0 + "deg", ClientVars.toClientPixelSize(-3) + "deg", 0 + "deg"],
        offset: [0, 0.5, 1]
    };
};

export const weaponWalkLeft = () => {
    return {
        translate: [ClientVars.toClientPixelSize(-2) + "px " + ClientVars.toClientPixelSize(-2) + "px", 0, ClientVars.toClientPixelSize(-2) + "px " + ClientVars.toClientPixelSize(-2) + "px"],
        rotate: [ClientVars.toClientPixelSize(-2) + "deg", 0 + "deg", ClientVars.toClientPixelSize(-2) + "deg"],
        offset: [0, 0.5, 1]
    };
};

export const weaponWalkRight = () => {
    return {
        translate: [ClientVars.toClientPixelSize(2) + "px " + ClientVars.toClientPixelSize(-2) + "px", 0, ClientVars.toClientPixelSize(2) + "px " + ClientVars.toClientPixelSize(-2) + "px"],
        rotate: [ClientVars.toClientPixelSize(2) + "deg", 0 + "deg", ClientVars.toClientPixelSize(2) + "deg"],
        offset: [0, 0.5, 1]
    };
};