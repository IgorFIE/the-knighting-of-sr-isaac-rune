import { BlockType } from "../../../enums/block-type";
import { DoorType } from "../../../enums/door-type";
import { SquareObject } from "../../../logic/collision-objects/square-object";
import { horizontalRoomBlocks, verticalRoomBlocks } from "../../../logic/game-variables";
import { createFloorBlock, createWallBlock, getSpecialDoorColors, getWallBlockColors } from "../../../utilities/block-util";
import { generateBox } from "../../../utilities/box-generator-util";
import { randomNumb } from "../../../utilities/general-util";
import { ClientVars } from "../../client-variables";

export const drawBlock = (pos, roomCtx, doorCtx, gameLevel, room) => {
    const block = room.backBlocks[pos.y][pos.x];
    const drawingBlock = new SquareObject(
        block.collisionObj.x * ClientVars.toClientPixelSize(1), block.collisionObj.y * ClientVars.toClientPixelSize(1),
        ClientVars.toClientPixelSize(block.collisionObj.w), ClientVars.toClientPixelSize(block.collisionObj.h));
    switch (block.blockType) {
        case BlockType.WALL: {
            const blockColors = getWallBlockColors(gameLevel);
            roomCtx.fillStyle = blockColors.md;
            roomCtx.fillRect(drawingBlock.x, drawingBlock.y, drawingBlock.w, drawingBlock.h);
            generateBox(roomCtx,
                convertToMapPixel(drawingBlock.x), convertToMapPixel(drawingBlock.y),
                convertToMapPixel(drawingBlock.w - ClientVars.toClientPixelSize(2)), convertToMapPixel(drawingBlock.h - ClientVars.toClientPixelSize(2)),
                ClientVars.toClientPixelSize(2), blockColors.dk, (x, y, endX, endY) => {
                    return randomNumb(100) < 3 || validateBlock(block, x, y, endX, endY,
                        (x, y, endX, endY) => x === 0 || y === endY,
                        (x, y, endX, endY) => x === 0,
                        (x, y, endX, endY) => x === 0 || y === endY,
                        (x, y, endX, endY) => x === 0 && y === endY,
                        (x, y, endX, endY) => y === endY,
                        (x, y, endX, endY) => x === endX - 3 || y === endY,
                        (x, y, endX, endY) => x === 0 || y === endY - 4
                    );
                });
            generateBox(roomCtx,
                convertToMapPixel(drawingBlock.x), convertToMapPixel(drawingBlock.y),
                convertToMapPixel(drawingBlock.w - ClientVars.toClientPixelSize(2)), convertToMapPixel(drawingBlock.h - ClientVars.toClientPixelSize(2)),
                ClientVars.toClientPixelSize(2), blockColors.lt, (x, y, endX, endY) => {
                    return validateBlock(block, x, y, endX, endY,
                        (x, y, endX, endY) => y === 0 || x === endX,
                        (x, y, endX, endY) => y === 0,
                        (x, y, endX, endY) => x === endX && y === 0,
                        (x, y, endX, endY) => x === endX || y === 0,
                        (x, y, endX, endY) => x === endY,
                        (x, y, endX, endY) => x === endX - 4 || y === 0,
                        (x, y, endX, endY) => x === endX || y === endY - 3
                    );
                });
            break;
        }

        case BlockType.DOOR_CLOSE: {
            const doorColors = getSpecialDoorColors(block.doorType);
            createDoor(doorCtx, drawingBlock, block, room, gameLevel, () => {
                doorCtx.fillStyle = doorColors.md;
                doorCtx.fillRect(drawingBlock.x, drawingBlock.y, ClientVars.toClientPixelSize(16), ClientVars.toClientPixelSize(16));
                generateBox(doorCtx,
                    convertToMapPixel(drawingBlock.x), convertToMapPixel(drawingBlock.y),
                    convertToMapPixel(drawingBlock.w - ClientVars.toClientPixelSize(2)), convertToMapPixel(drawingBlock.h - ClientVars.toClientPixelSize(2)),
                    ClientVars.toClientPixelSize(2), doorColors.lt, (x, y, endX, endY) => {
                        return (y === 0 && block.roomPos.y !== 1 && block.roomPos.y < Math.round(verticalRoomBlocks) - 1) || // Top lines
                            (x === endX && block.roomPos.x > 0 && block.roomPos.x !== Math.round(horizontalRoomBlocks) - 2) || // right Lines
                            (y < 3 && block.roomPos.y === 0) || // topDoorFrame
                            (x > endX - 3 && block.roomPos.x === Math.round(horizontalRoomBlocks) - 1)
                            ;
                    });
                generateBox(doorCtx,
                    convertToMapPixel(drawingBlock.x), convertToMapPixel(drawingBlock.y),
                    convertToMapPixel(drawingBlock.w - ClientVars.toClientPixelSize(2)), convertToMapPixel(drawingBlock.h - ClientVars.toClientPixelSize(2)),
                    ClientVars.toClientPixelSize(2), doorColors.dk, (x, y, endX, endY) => {
                        return (y === endY && block.roomPos.y > 0 && block.roomPos.y !== Math.round(verticalRoomBlocks) - 2) || //Bottom lines
                            (x === 0 && ((block.roomPos.x === 0 || block.roomPos.x === Math.round(horizontalRoomBlocks) - 2) ||
                                (block.roomPos.x > 1 && block.roomPos.x < Math.round(horizontalRoomBlocks) - 2))) || // left lines
                            (y < 2 && block.roomPos.y === 0) || // top door frame
                            (y > endY - 2 && block.roomPos.y === Math.round(verticalRoomBlocks) - 1) || // bottom door frame
                            (x < 2 && block.roomPos.x === 0) || // left door frame
                            (x > endX - 2 && block.roomPos.x === Math.round(horizontalRoomBlocks) - 1) // right door frame
                            ;
                    });
                if (block.roomPos.y < 3 || block.roomPos.y > Math.round(verticalRoomBlocks) - 3) {
                    createDoorFrame(doorCtx, drawingBlock.x, drawingBlock.y, ClientVars.toClientPixelSize(16), ClientVars.toClientPixelSize(8));
                } else {
                    createDoorFrame(doorCtx, drawingBlock.x + ClientVars.toClientPixelSize(8), drawingBlock.y, ClientVars.toClientPixelSize(8), ClientVars.toClientPixelSize(16));
                }

                if (block.doorType === DoorType.TREASURE) {
                    if ((block.roomPos.x === 1 && room.backBlocks[block.roomPos.y - 1][block.roomPos.x].blockType == BlockType.FLOOR && room.backBlocks[block.roomPos.y + 1][block.roomPos.x].blockType == BlockType.FLOOR) ||
                        (block.roomPos.x === Math.round(horizontalRoomBlocks) - 1 && room.backBlocks[block.roomPos.y - 2][block.roomPos.x].blockType == BlockType.FLOOR && room.backBlocks[block.roomPos.y + 2][block.roomPos.x].blockType == BlockType.FLOOR)) {
                        createKeyHole(doorCtx, drawingBlock.x, drawingBlock.y + ClientVars.toClientPixelSize(4));
                    }
                    if ((block.roomPos.y === 0 && room.backBlocks[block.roomPos.y][block.roomPos.x - 2].blockType == BlockType.FLOOR && room.backBlocks[block.roomPos.y][block.roomPos.x + 2].blockType == BlockType.FLOOR) ||
                        (block.roomPos.y === Math.round(verticalRoomBlocks) - 2 && room.backBlocks[block.roomPos.y][block.roomPos.x - 1].blockType == BlockType.FLOOR && room.backBlocks[block.roomPos.y][block.roomPos.x + 1].blockType == BlockType.FLOOR)) {
                        createKeyHole(doorCtx, drawingBlock.x + ClientVars.toClientPixelSize(6), drawingBlock.y + ClientVars.toClientPixelSize(10));
                    }
                }
            });
            break;
        }

        case BlockType.DOOR_OPEN:
            createDoor(doorCtx, drawingBlock, block, room, gameLevel);
            break;

        case BlockType.FLOOR:
            createFloorBlock(roomCtx, drawingBlock.x, drawingBlock.y, gameLevel);
            break;
    }
}

const validateBlock = (block, x, y, endX, endY, insideFn, topLeftFn, bottomLeftFn, topRightFn, bottomRightFn, topBottomFn, leftRightFn) => {
    if (block.roomPos.x > 0 && block.roomPos.x < horizontalRoomBlocks - 1 && block.roomPos.y > 0 && block.roomPos.y < verticalRoomBlocks - 1) {
        return insideFn(x, y, endX, endY);
    } else if (block.roomPos.x === 0 && block.roomPos.y === 0) {
        return topLeftFn(x, y, endX, endY);
    } else if (block.roomPos.x === 0 && block.roomPos.y >= verticalRoomBlocks - 1) {
        return bottomLeftFn(x, y, endX, endY);
    } else if (block.roomPos.x >= horizontalRoomBlocks - 1 && block.roomPos.y === 0) {
        return topRightFn(x, y, endX, endY);
    } else if (block.roomPos.x >= horizontalRoomBlocks - 1 && block.roomPos.y >= verticalRoomBlocks - 1) {
        return bottomRightFn(x, y, endX, endY);
    } else if ((block.roomPos.y === 0 && block.roomPos.x > 0 && block.roomPos.x < horizontalRoomBlocks - 1) || block.roomPos.y >= verticalRoomBlocks - 1) {
        return topBottomFn(x, y, endX, endY);
    } else {
        return leftRightFn(x, y, endX, endY);
    }
}

const createDoor = (ctx, drawingBlock, block, room, gameLevel, elseFn) => {
    if ((block.roomPos.x === room.backBlocks[0].length - 1 || block.roomPos.x === 0) &&
        block.roomPos.y - 1 > 0 && room.backBlocks[block.roomPos.y - 1][block.roomPos.x].blockType == BlockType.WALL) {
        createWallBlock(ctx, drawingBlock.x, drawingBlock.y - ClientVars.toClientPixelSize(8), gameLevel);
        createDoorFrame(ctx, drawingBlock.x, drawingBlock.y + ClientVars.toClientPixelSize(8), ClientVars.toClientPixelSize(16), ClientVars.toClientPixelSize(8));
    } else if ((block.roomPos.x === room.backBlocks[0].length - 1 || block.roomPos.x === 0) &&
        block.roomPos.y + 1 < room.backBlocks.length && room.backBlocks[block.roomPos.y + 1][block.roomPos.x].blockType == BlockType.WALL) {
        createWallBlock(ctx, drawingBlock.x, drawingBlock.y + ClientVars.toClientPixelSize(8), gameLevel);
        createDoorFrame(ctx, drawingBlock.x, drawingBlock.y, ClientVars.toClientPixelSize(16), ClientVars.toClientPixelSize(8));
    } else if ((block.roomPos.y === room.backBlocks.length - 1 || block.roomPos.y === 0) &&
        block.roomPos.x - 1 > 0 && room.backBlocks[block.roomPos.y][block.roomPos.x - 1].blockType == BlockType.WALL) {
        createWallBlock(ctx, drawingBlock.x - ClientVars.toClientPixelSize(8), drawingBlock.y, gameLevel);
        createDoorFrame(ctx, drawingBlock.x + ClientVars.toClientPixelSize(8), drawingBlock.y, ClientVars.toClientPixelSize(8), ClientVars.toClientPixelSize(16));
    } else if ((block.roomPos.y === room.backBlocks.length - 1 || block.roomPos.y === 0) &&
        block.roomPos.x + 1 < room.backBlocks[0].length && room.backBlocks[block.roomPos.y][block.roomPos.x + 1].blockType == BlockType.WALL) {
        createWallBlock(ctx, drawingBlock.x + ClientVars.toClientPixelSize(8), drawingBlock.y, gameLevel);
        createDoorFrame(ctx, drawingBlock.x, drawingBlock.y, ClientVars.toClientPixelSize(8), ClientVars.toClientPixelSize(16));
    } else {
        if (elseFn) elseFn();
    }
}

const createDoorFrame = (ctx, x, y, w, h) => {
    ctx.fillStyle = "#843d0d";
    ctx.fillRect(x, y, w, h);

    ctx.fillStyle = "#865433";
    ctx.fillRect(x, y, w, ClientVars.toClientPixelSize(2));
    ctx.fillRect(x + w - ClientVars.toClientPixelSize(2), y, ClientVars.toClientPixelSize(2), h);

    ctx.fillStyle = "#2f1519";
    ctx.fillRect(x + ClientVars.toClientPixelSize(w / ClientVars.toClientPixelSize(2)), y + ClientVars.toClientPixelSize((h / ClientVars.toClientPixelSize(2)) - 2), ClientVars.toClientPixelSize(2), ClientVars.toClientPixelSize(2));
    ctx.fillRect(x, y + h - ClientVars.toClientPixelSize(2), w, ClientVars.toClientPixelSize(2));
    ctx.fillRect(x, y, ClientVars.toClientPixelSize(2), h);
}

const createKeyHole = (ctx, x, y) => {
    ctx.fillStyle = "#000000";
    ctx.fillRect(x, y, ClientVars.toClientPixelSize(6), ClientVars.toClientPixelSize(6));

    ctx.fillStyle = "#3e3846";
    ctx.fillRect(x, y + ClientVars.toClientPixelSize(6), ClientVars.toClientPixelSize(6), ClientVars.toClientPixelSize(2));
    ctx.fillRect(x - ClientVars.toClientPixelSize(2), y, ClientVars.toClientPixelSize(2), ClientVars.toClientPixelSize(6));

    ctx.fillStyle = "#999a9e";
    ctx.fillRect(x, y - ClientVars.toClientPixelSize(2), ClientVars.toClientPixelSize(6), ClientVars.toClientPixelSize(2));
    ctx.fillRect(x + ClientVars.toClientPixelSize(6), y, ClientVars.toClientPixelSize(2), ClientVars.toClientPixelSize(6));
}

const convertToMapPixel = (value, amount = 2) => {
    return value / ClientVars.toClientPixelSize(amount);
}