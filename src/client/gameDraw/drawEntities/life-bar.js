import { HeartSprite } from "../../../assets/sprites";
import { convertTextToPixelArt, drawPixelTextInCanvas } from "../../../assets/text";
import { heartLifeVal } from "../../../logic/game-variables";
import { genSmallBox } from "../../../utilities/box-generator-util";
import { createElem, setElemSize } from "../../../utilities/element-util";
import { drawSprite } from "../../../utilities/pixel-util";
import { ClientVars } from "../../client-variables";

export class LifeBar {
    constructor(totalLife, isPlayer, isBoss, parentCanv, currentLife, name) {
        this.isPlayer = isPlayer;
        this.isBoss = isBoss;
        this.totalLife = totalLife;
        this.life = currentLife || totalLife;
        this.parentCanv = parentCanv;
        this.parentRect;
        this.playerName = name;

        this.playerNameCanv = name ? createElem(parentCanv, "canvas", null, ["playerName"]) : null;
        this.lifeBackgroundCanv = createElem(isPlayer ? ClientVars.gameDiv : parentCanv, "canvas", null, ["heartB"]);
        this.lifeCanv = createElem(isPlayer ? ClientVars.gameDiv : parentCanv, "canvas", null, ["heart"]);

        this.init();
    }

    init() {
        this.draw();
        this.updateLifeBar = true;
        this.update();
    }

    addLife() {
        this.life += heartLifeVal;
        this.life = this.life > this.totalLife ? this.totalLife : this.life;
        this.draw();
        this.updateLifeBar = true;
    }

    takeDmg(amount) {
        this.life -= this.isBoss ? amount / 2 : amount;
        this.life = this.life < 0 ? 0 : this.life;
        this.updateLifeBar = true;
    }

    update() {
        if (!this.isPlayer) {
            this.parentRect = this.parentCanv.getBoundingClientRect();
            if (this.playerNameCanv) {
                this.playerNameCanv.style.translate = ((this.parentRect.width / 2) - (this.playerNameCanv.width / 2)) + 'px ' +
                    -ClientVars.toClientPixelSize(25) + 'px';
            }
            this.lifeBackgroundCanv.style.translate = ((this.parentRect.width / 2) - (this.lifeCanv.width / 2)) + 'px ' +
                -ClientVars.toClientPixelSize(13) + 'px';
            this.lifeCanv.style.translate = ((this.parentRect.width / 2) - (this.lifeCanv.width / 2) + ClientVars.toClientPixelSize(2)) + 'px ' +
                -ClientVars.toClientPixelSize(13) + 'px';
        }
        if (this.updateLifeBar) {
            this.updateLifeBar = false;
            this.lifeCanv.getContext("2d").clearRect((this.life * this.lifeCanv.width) / this.totalLife, 0, this.lifeCanv.width, this.lifeCanv.height);
        }
    }

    draw() {
        setElemSize(this.lifeBackgroundCanv, ClientVars.toClientPixelSize(7 * (this.totalLife / heartLifeVal) + (this.totalLife / heartLifeVal) + 3), ClientVars.toClientPixelSize(11));
        setElemSize(this.lifeCanv, ClientVars.toClientPixelSize(8 * (this.totalLife / heartLifeVal) - 1), ClientVars.toClientPixelSize(11));

        if (this.isPlayer) {
            this.lifeBackgroundCanv.style.translate = ClientVars.toClientPixelSize(12) + 'px ' + ClientVars.toClientPixelSize(24) + 'px';
            this.lifeCanv.style.translate = ClientVars.toClientPixelSize(14) + 'px ' + ClientVars.toClientPixelSize(24) + 'px';
        }

        const lifeBackgroundCtx = this.lifeBackgroundCanv.getContext("2d");
        const lifeCtx = this.lifeCanv.getContext("2d");

        lifeBackgroundCtx.clearRect(0, 0, this.lifeBackgroundCanv.width, this.lifeBackgroundCanv.height);
        lifeCtx.clearRect(0, 0, this.lifeCanv.width, this.lifeCanv.height);

        genSmallBox(lifeBackgroundCtx, 0, 0, (8 * (this.totalLife / heartLifeVal)) + 2, 10, ClientVars.toClientPixelSize(1), "#00000066", "#100f0f66");

        for (let i = 0; i < this.totalLife / heartLifeVal; i++) {
            drawSprite(lifeBackgroundCtx, HeartSprite, ClientVars.toClientPixelSize(1), 2 + (8 * i), 2, { "ho": "#2f1519", "hi": "#100f0f" });
            drawSprite(lifeCtx, HeartSprite, ClientVars.toClientPixelSize(1), (8 * i), 2, { "ho": "#edeef7", "hi": "#a80000" });
        }

        if (this.playerNameCanv) {
            const textwidth = this.playerName.length * 5;
            setElemSize(this.playerNameCanv, ClientVars.toClientPixelSize(textwidth + 1), ClientVars.toClientPixelSize(11));
            const playerNameCtx = this.playerNameCanv.getContext("2d");
            playerNameCtx.clearRect(0, 0, this.playerNameCanv.width, this.playerNameCanv.height);
            genSmallBox(playerNameCtx, 0, 0, textwidth, 10, ClientVars.toClientPixelSize(1), "#00000066", "#100f0f66");
            drawPixelTextInCanvas(convertTextToPixelArt(this.playerName), playerNameCtx, ClientVars.toClientPixelSize(1), textwidth / 2, 10 / 2, "#edeef7", 1);
        }
    }
}