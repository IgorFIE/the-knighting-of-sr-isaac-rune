import { KeySprite, KnightSprite } from "../../../assets/sprites";
import { InputKey } from "../../../enums/movement-type";
import { WeaponType } from "../../../enums/weapon-type";
import { CircleObject } from "../../../logic/collision-objects/circle-object";
import { genSmallBox } from "../../../utilities/box-generator-util";
import { createElem, setElemSize } from "../../../utilities/element-util";
import { drawSprite } from "../../../utilities/pixel-util";
import { ClientVars } from "../../client-variables";
import { walk, weaponWalkLeft, weaponWalkRight } from "../../drawUtilities/animation-utilities";
import { LifeBar } from "./life-bar";
import { WeaponDraw } from "./weapon-draw";


export class PlayerDraw {
    constructor(playerInfo, isYourPlayer) {
        this.playerColors = playerInfo.colors;

        this.collisionObj = new CircleObject(0, 0, 0);

        this.div = createElem(ClientVars.gameDiv, "div", null, ["player"]);

        this.shadowCanv = createElem(this.div, "canvas");
        setElemSize(this.shadowCanv, ClientVars.toClientPixelSize(2) * 7, ClientVars.toClientPixelSize(2) * 6);
        this.shadowCanv.style.translate = -ClientVars.toClientPixelSize(4) + 'px ' + ClientVars.toClientPixelSize(8) + 'px';

        this.playerCanv = createElem(this.div, "canvas");
        setElemSize(this.playerCanv, KnightSprite[0].length * ClientVars.toClientPixelSize(2), KnightSprite.length * ClientVars.toClientPixelSize(2));

        this.keyCanv = createElem(ClientVars.gameDiv, "canvas", null, ["hidden"]);
        setElemSize(this.keyCanv, (KeySprite[0].length * ClientVars.toClientPixelSize(2)) + ClientVars.toClientPixelSize(4), (KeySprite.length * ClientVars.toClientPixelSize(2)) + ClientVars.toClientPixelSize(4));
        this.keyCanv.style.translate = ClientVars.toClientPixelSize(12) + 'px ' + ClientVars.toClientPixelSize(37) + 'px';

        this.playerLeftWeapon = new WeaponDraw(playerInfo.leftHand.weapon || WeaponType.FIST, -1, this, this.playerColors.hd, null, true);
        this.playerRightWeapon = new WeaponDraw(playerInfo.rightHand.weapon || WeaponType.FIST, 1, this, this.playerColors.hd, null, true);

        this.walkAnim = this.playerCanv.animate(walk(), { duration: 160, fill: 'forwards' });

        this.draw();

        const playerName = Rune.getPlayerInfo(playerInfo.playerId).displayName;
        this.lifeBar = new LifeBar(playerInfo.life, isYourPlayer, false, this.div, playerInfo.life, isYourPlayer ? "" : playerName);
        this.lifeBar.init();

        this.updateDraw(playerInfo.collisionObj);
    }

    draw() {
        const shadowCtx = this.shadowCanv.getContext("2d");
        genSmallBox(shadowCtx, 0, 0, 6, 5, ClientVars.toClientPixelSize(2), "#00000033", "#00000033");

        const playerCtx = this.playerCanv.getContext("2d");
        drawSprite(playerCtx, KnightSprite, ClientVars.toClientPixelSize(2), 0, 0, this.playerColors);

        const keyCtx = this.keyCanv.getContext("2d");
        genSmallBox(keyCtx, 0, 0, 4, 8, ClientVars.toClientPixelSize(2), "#00000066", "#100f0f66");
        drawSprite(keyCtx, KeySprite, ClientVars.toClientPixelSize(2), 1, 1);

        let playerRect = this.playerCanv.getBoundingClientRect();
        this.div.style.width = playerRect.width + "px";
        this.div.style.height = playerRect.height + "px";
        this.div.style.transformOrigin = "70% 95%";
    }

    updateDraw(collisionObj) {
        this.collisionObj.x = ClientVars.toClientPixelSize(collisionObj.x);
        this.collisionObj.y = ClientVars.toClientPixelSize(collisionObj.y);
        this.collisionObj.r = ClientVars.toClientPixelSize(collisionObj.r);

        this.updateDrawPosition(this.collisionObj);
        this.lifeBar.update();
    }

    updateDrawPosition() {
        this.div.style.translate = (this.collisionObj.x - (KnightSprite[0].length * ClientVars.toClientPixelSize(2)) / 2) + 'px ' +
            (this.collisionObj.y - (KnightSprite.length * ClientVars.toClientPixelSize(2)) / 4 * 3) + 'px';
    }

    handleInputAnimations(keys) {
        this.handleMovementInput(keys);
        this.handleAtkInput(keys);
    }

    handleMovementInput(keys) {
        if (this.collectMovementKeys(keys).length > 0 && this.walkAnim.playState === "finished") {
            this.walkAnim.play();
            this.playerLeftWeapon.weaponCanv.animate(weaponWalkLeft(), { duration: 160 });
            this.playerRightWeapon.weaponCanv.animate(weaponWalkRight(), { duration: 160 });
        }
    }

    collectMovementKeys(keys) {
        return Object.keys(keys).filter((key) => (
            key === InputKey.UP || key === InputKey.DOWN || key === InputKey.LEFT || key === InputKey.RIGHT
        ) && keys[key]);
    }

    handleAtkInput(keys) {
        (keys[InputKey.LEFT_ATK]) && this.playerLeftWeapon.action();
        (keys[InputKey.RIGHT_ATK]) && this.playerRightWeapon.action();
        this.playerLeftWeapon.update();
        this.playerRightWeapon.update();
    }
}