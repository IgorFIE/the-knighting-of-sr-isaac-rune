import { genericBlockSize, horizontalRoomBlocks, verticalRoomBlocks } from "../../../logic/game-variables";
import { createElem, setElemSize } from "../../../utilities/element-util";
import { ClientVars } from "../../client-variables";
import { drawBlock } from "./block-draw";

export class RoomDraw {
    constructor(room, gameLevel) {
        this.roomDiv = createElem(ClientVars.gameDiv, "div", null, ["room"]);
        this.roomCanv = createElem(this.roomDiv, "canvas");
        this.doorCanv = createElem(this.roomDiv, "canvas");
        this.envCanv = createElem(this.roomDiv, "canvas");

        this.drawRoom(room, gameLevel);
    }

    drawRoom(room, gameLevel) {

        setElemSize(this.roomCanv, ClientVars.toClientPixelSize(horizontalRoomBlocks * genericBlockSize), ClientVars.toClientPixelSize(verticalRoomBlocks * genericBlockSize));
        setElemSize(this.doorCanv, ClientVars.toClientPixelSize(horizontalRoomBlocks * genericBlockSize), ClientVars.toClientPixelSize(verticalRoomBlocks * genericBlockSize));
        setElemSize(this.envCanv, ClientVars.toClientPixelSize(horizontalRoomBlocks * genericBlockSize), ClientVars.toClientPixelSize(verticalRoomBlocks * genericBlockSize));

        // this.centerDiv();

        const roomCtx = this.roomCanv.getContext("2d");
        const doorCtx = this.doorCanv.getContext("2d");
        // const envCtx = this.roomCanv.getContext("2d");

        room.floors.forEach(pos => drawBlock(pos, roomCtx, doorCtx, gameLevel, room));
        this.drawRoomShadows(room, roomCtx);
        room.walls.forEach(pos => drawBlock(pos, roomCtx, doorCtx, gameLevel, room));
        room.doors.forEach(pos => drawBlock(pos, roomCtx, doorCtx, gameLevel, room));
        // room.spikesBlocks.forEach(spikes => spikes.draw());
        // room.stonesBlocks.forEach(stone => stone.draw());
    }

    centerDiv() {
        this.roomDiv.style.translate = ((window.innerWidth - this.roomCanv.width) / 2) + 'px ' +
            ((window.innerHeight - this.roomCanv.height) / 2) + 'px'
    }

    drawRoomShadows(room, ctx) {
        ctx.fillStyle = "#00000033";
        ctx.fillRect(0, 0, ClientVars.toClientPixelSize(34), this.roomCanv.height);
        ctx.fillRect(ClientVars.toClientPixelSize(34), 0, this.roomCanv.width, ClientVars.toClientPixelSize(34));
        ctx.fillRect(this.roomCanv.width - ClientVars.toClientPixelSize(34), ClientVars.toClientPixelSize(34), ClientVars.toClientPixelSize(34), this.roomCanv.height);
        ctx.fillRect(ClientVars.toClientPixelSize(34),
            this.roomCanv.height - ClientVars.toClientPixelSize(34),
            this.roomCanv.width - ClientVars.toClientPixelSize(34 * 2),
            ClientVars.toClientPixelSize(34));
    }

    updateDraw() {

    }

    show() {
        this.roomDiv.classList.remove("hidden");
    }

    hide() {
        this.roomDiv.classList.add("hidden");
    }
}