import { WeaponType, getWeaponSprite, isProjectileWeapon } from "../../../enums/weapon-type";
import { createElem, setElemSize } from "../../../utilities/element-util";
import { drawSprite } from "../../../utilities/pixel-util";
import { ClientVars } from "../../client-variables";


export class WeaponDraw {
    constructor(weaponType, handDir, parent, color, size, isPlayer) {
        this.color = color;
        this.handDir = handDir;
        this.weaponType = weaponType;
        this.parentDiv = parent.div;
        this.size = size || 2;

        this.sprite = getWeaponSprite(this.weaponType);
        this.isPerformingAction = false;
        this.isPlayer = isPlayer;

        this.weaponDiv = createElem(this.parentDiv, "div", null, ["weapon"]);
        this.weaponCanv = createElem(this.weaponDiv, "canvas");

        setElemSize(this.weaponCanv, this.sprite[0].length * ClientVars.toClientPixelSize(this.size), this.sprite.length * ClientVars.toClientPixelSize(this.size));

        this.atkAnimation = this.getWeaponAnimation();
        this.relativePos = this.getRelativePos();
        this.setWeaponPos();
        this.originAtkLine = this.getWeaponAtkLine();
        this.atkLine = {};

        this.draw();
    }

    getWeaponAnimation() {
        switch (this.weaponType) {
            case WeaponType.FIST:
                return this.weaponCanv.animate({
                    transform: ["translateY(0)", "translateY( " + ClientVars.toClientPixelSize(this.size * 3) + "px)", "translateY(0)"],
                    easing: ["ease-in", "ease-out", "ease-out"],
                    offset: [0, 0.5, 1]
                }, 100);
            case WeaponType.MORNING_STAR:
                return this.weaponCanv.animate({
                    transform: ["rotate(0)", "rotate(" + 360 * this.handDir + "deg)"],
                    easing: ["ease-in", "ease-out"],
                    offset: [0, 1]
                }, 175);
            case WeaponType.SHIELD:
                return this.weaponCanv.animate({
                    transform: ["translateY(0) scale(1)", "translateY( " + ClientVars.toClientPixelSize(this.size * 3) + "px)  scale(2)", "translateY(0) scale(1)"],
                    easing: ["ease-in", "ease-out", "ease-out"],
                    offset: [0, 0.5, 1]
                }, 200);
            case WeaponType.SWORD:
                return this.weaponCanv.animate({
                    transform: ["rotate(0)", "rotate(" + 180 * this.handDir + "deg)", "rotate(0)"],
                    easing: ["ease-in", "ease-out", "ease-in"],
                    offset: [0, 0.25, 1]
                }, 250);
            case WeaponType.TROWING_AXE:
                return this.weaponCanv.animate({
                    transform: ["translateY(0) rotate(0)", "translateY(" + ClientVars.toClientPixelSize(8) + "px) rotate(" + 180 * this.handDir + "deg)", "translateY(0) rotate(0)"],
                    easing: ["ease-in", "ease-out", "ease-in"],
                    offset: [0, 0.15, 1]
                }, 1200);
            case WeaponType.AXE:
                return this.weaponCanv.animate({
                    transform: ["rotate(0)", "rotate(" + 225 * this.handDir + "deg)", "rotate(0)"],
                    easing: ["ease-in", "ease-out", "ease-in"],
                    offset: [0, 0.35, 1]
                }, 300);
            case WeaponType.TROWING_KNIVE:
                return this.weaponCanv.animate({
                    transform: ["translateY(0) rotate(0)", "translateY(" + ClientVars.toClientPixelSize(8) + "px) rotate(" + 225 * this.handDir + "deg)", "translateY(0) rotate(0)"],
                    easing: ["ease-in", "ease-out", "ease-in"],
                    offset: [0, 0.35, 1]
                }, 300);
            case WeaponType.SPEAR:
                return this.weaponCanv.animate({
                    transform: ["translateY(0)", "translateY( " + ClientVars.toClientPixelSize(this.size * 12) + "px)", "translateY(0)"],
                    easing: ["ease-in", "ease-out", "ease-out"],
                    offset: [0, 0.5, 1]
                }, 500);
            case WeaponType.HALBERD:
                return this.weaponCanv.animate({
                    transform: ["rotate(0) translateY(0)", "rotate(" + -20 * this.handDir + "deg) translateY( " + -ClientVars.toClientPixelSize(this.size * 6) + "px)", "rotate(0) translateY(0)"],
                    easing: ["ease-in", "ease-out", "ease-out"],
                    offset: [0, 0.45, 1]
                }, 500);
            case WeaponType.HAMMER:
                return this.weaponCanv.animate({
                    transform: ["rotate(0)", "rotate(" + -180 * this.handDir + "deg)", "rotate(0)"],
                    easing: ["ease-in", "ease-out", "ease-in"],
                    offset: [0, 0.40, 1]
                }, 750);
            case WeaponType.CROSSBOW:
                return this.weaponCanv.animate({
                    transform: ["translateY(0)", "translateY( " + ClientVars.toClientPixelSize(this.size * 4) + "px)", "translateY(0)"],
                    easing: ["ease-in", "ease-out", "ease-out"],
                    offset: [0, 0.1, 1]
                }, 900);
            case WeaponType.GREATSWORD:
                return this.weaponCanv.animate({
                    transform: ["rotate(0)", "rotate(" + 360 * this.handDir + "deg)"],
                    easing: ["ease-in", "ease-out"],
                    offset: [0, 1]
                }, 1000);
        }
    }

    getRelativePos() {
        switch (this.weaponType) {
            case WeaponType.FIST:
                return { x: ClientVars.toClientPixelSize(this.handDir === -1 ? -this.size : this.size * 2), y: ClientVars.toClientPixelSize(this.size * 4.5) };
            case WeaponType.SHIELD:
                return { x: ClientVars.toClientPixelSize(this.size * 2 * this.handDir), y: ClientVars.toClientPixelSize(this.size * 3) };
            case WeaponType.SWORD:
                return { x: ClientVars.toClientPixelSize(this.size * 3 * this.handDir), y: -ClientVars.toClientPixelSize(2), r: -90 };
            case WeaponType.GREATSWORD:
                return { x: ClientVars.toClientPixelSize(this.handDir === - 1 ? -(this.size * 10) : this.size * 8), y: -ClientVars.toClientPixelSize(this.handDir === - 1 ? this.size * 0.5 : this.size * 5.5), r: 45 * this.handDir };
            case WeaponType.TROWING_KNIVE:
            case WeaponType.SPEAR:
                return { x: ClientVars.toClientPixelSize(this.handDir === - 1 ? -(this.size * 2) : this.size * 4), y: -ClientVars.toClientPixelSize(this.size) };
            case WeaponType.HALBERD:
                return { x: ClientVars.toClientPixelSize(this.handDir === - 1 ? -(this.size * 4) : this.size * 4), y: -ClientVars.toClientPixelSize(this.size * 8), r: 0 };
            case WeaponType.HAMMER:
                return { x: ClientVars.toClientPixelSize(this.handDir === - 1 ? -(this.size * 4) : this.size * 9), y: -ClientVars.toClientPixelSize(this.handDir === - 1 ? -this.size * 8 : -this.size * 6), r: 135 * this.handDir };
            case WeaponType.AXE:
                return { x: ClientVars.toClientPixelSize(this.handDir === - 1 ? -(this.size * 6) : this.size * 7), y: -ClientVars.toClientPixelSize(this.handDir === - 1 ? -this.size * 2 : 0), r: 45 * this.handDir };
            case WeaponType.MORNING_STAR:
                return { x: ClientVars.toClientPixelSize(this.handDir === - 1 ? -(this.size * 5) : this.size * 6), y: -ClientVars.toClientPixelSize(this.handDir === - 1 ? -this.size * 4 : -this.size * 2), r: 45 * this.handDir };
            case WeaponType.CROSSBOW:
                return { x: ClientVars.toClientPixelSize(this.handDir === - 1 ? -(this.size * 10) : this.size * 8), y: ClientVars.toClientPixelSize(this.handDir === - 1 ? this.size * 2 : -this.size * 3), r: 45 * this.handDir };

            case WeaponType.TROWING_AXE:
                return { x: ClientVars.toClientPixelSize(this.handDir === - 1 ? -(this.size * 6) : this.size * 8), y: ClientVars.toClientPixelSize(this.handDir === - 1 ? this.size * 2 : this.size * 1), r: 45 * this.handDir };
        }
    }

    setWeaponPos() {
        this.weaponDiv.style.translate = this.relativePos.x + 'px ' + this.relativePos.y + 'px';
        switch (this.weaponType) {
            case WeaponType.SWORD:
                this.weaponCanv.style.transformOrigin = "50% 90%";
                break;
            case WeaponType.GREATSWORD:
            case WeaponType.HAMMER:
            case WeaponType.AXE:
            case WeaponType.MORNING_STAR:
            case WeaponType.CROSSBOW:
            case WeaponType.TROWING_KNIVE:
            case WeaponType.TROWING_AXE:
            case WeaponType.HALBERD:
                this.weaponDiv.style.rotate = this.relativePos.r + 'deg';
                this.weaponCanv.style.transformOrigin = "50% 100%";
                break;
        }
    }

    getWeaponAtkLine() {
        switch (this.weaponType) {
            case WeaponType.FIST:
            case WeaponType.SHIELD:
                return [
                    { x: this.relativePos.x, y: this.relativePos.y + (this.sprite.length * ClientVars.toClientPixelSize(this.size)) },
                    { x: this.relativePos.x + (this.sprite[0].length * ClientVars.toClientPixelSize(this.size)), y: this.relativePos.y + (this.sprite.length * ClientVars.toClientPixelSize(this.size)) },
                ];
            case WeaponType.SWORD:
                return [
                    { x: this.relativePos.x + (this.sprite[0].length / 2 * ClientVars.toClientPixelSize(this.size)), y: this.relativePos.y + ((90 * this.sprite.length * ClientVars.toClientPixelSize(this.size)) / 100) }
                ];
            case WeaponType.GREATSWORD:
            case WeaponType.HAMMER:
            case WeaponType.AXE:
            case WeaponType.MORNING_STAR: {
                const anglePoint1 = this.retrieveAnglePoint(
                    this.relativePos.x,
                    this.relativePos.y,
                    this.sprite.length * ClientVars.toClientPixelSize(this.size),
                    this.relativePos.r + 72.5
                );
                const anglePoint2 = this.retrieveAnglePoint(
                    this.relativePos.x,
                    this.relativePos.y,
                    this.sprite[0].length / 2 * ClientVars.toClientPixelSize(this.size),
                    this.relativePos.r
                );
                return [
                    { x: anglePoint1.x, y: anglePoint1.y },
                    { x: anglePoint2.x, y: anglePoint2.y },
                ];
            }
            case WeaponType.HALBERD:
                return [
                    { x: this.relativePos.x + (this.sprite[0].length / 2) * ClientVars.toClientPixelSize(this.size), y: this.relativePos.y + (this.sprite.length * ClientVars.toClientPixelSize(this.size) / 2) },
                    { x: this.relativePos.x + (this.sprite[0].length / 2) * ClientVars.toClientPixelSize(this.size), y: this.relativePos.y },
                ];
            case WeaponType.SPEAR:
                return [
                    { x: this.relativePos.x + ClientVars.toClientPixelSize(this.size / 2), y: this.relativePos.y + (this.sprite.length * ClientVars.toClientPixelSize(this.size) / 2) },
                    { x: this.relativePos.x + ClientVars.toClientPixelSize(this.size / 2), y: this.relativePos.y + (this.sprite.length * ClientVars.toClientPixelSize(this.size)) },
                ];
        }
    }

    getUpdatedWeaponAtkLine(box, transform) {
        switch (this.weaponType) {
            case WeaponType.FIST:
                return [
                    { x: box.x + this.originAtkLine[0].x, y: box.y + this.originAtkLine[0].y + (transform.f * 1.5) },
                    { x: box.x + this.originAtkLine[1].x, y: box.y + this.originAtkLine[1].y + (transform.f * 1.5) }
                ];
            case WeaponType.SHIELD:
                return [
                    { x: box.x + this.originAtkLine[0].x - (ClientVars.toClientPixelSize(this.size) * (transform.m11 - 1)), y: box.y + this.originAtkLine[0].y + transform.f * transform.m11 },
                    { x: box.x + this.originAtkLine[1].x + (ClientVars.toClientPixelSize(this.size) * (transform.m11 - 1)), y: box.y + this.originAtkLine[1].y + transform.f * transform.m11 }
                ];
            case WeaponType.SWORD: {
                const swordAnglePoint = this.retrieveAnglePoint(
                    box.x + this.originAtkLine[0].x,
                    box.y + this.originAtkLine[0].y,
                    this.sprite.length * ClientVars.toClientPixelSize(this.size),
                    (Math.atan2(transform.b, transform.a) * 180 / Math.PI) + this.relativePos.r
                );
                return [
                    { x: box.x + this.originAtkLine[0].x, y: box.y + this.originAtkLine[0].y },
                    { x: swordAnglePoint.x, y: swordAnglePoint.y }
                ];
            }
            case WeaponType.GREATSWORD:
            case WeaponType.HAMMER:
            case WeaponType.AXE:
            case WeaponType.MORNING_STAR: {
                const rotationAnglePoint = this.retrieveAnglePoint(
                    box.x + this.originAtkLine[0].x,
                    box.y + this.originAtkLine[0].y,
                    this.sprite.length * ClientVars.toClientPixelSize(this.size),
                    (Math.atan2(transform.b, transform.a) * 180 / Math.PI) + this.relativePos.r - 90
                );
                return [
                    { x: box.x + this.originAtkLine[0].x, y: box.y + this.originAtkLine[0].y },
                    { x: rotationAnglePoint.x, y: rotationAnglePoint.y }
                ];
            }
            case WeaponType.HALBERD: {
                const topPoint = this.retrieveAnglePoint(
                    box.x + this.originAtkLine[1].x,
                    box.y + this.originAtkLine[1].y + (this.sprite.length * ClientVars.toClientPixelSize(this.size)) + (transform.f * 1.1),
                    this.sprite.length * ClientVars.toClientPixelSize(this.size),
                    (Math.atan2(transform.b, transform.a) * 180 / Math.PI) + this.relativePos.r - 90
                );
                const bottomPoint = this.retrieveAnglePoint(
                    box.x + this.originAtkLine[1].x,
                    box.y + this.originAtkLine[1].y + (this.sprite.length * ClientVars.toClientPixelSize(this.size)) + (transform.f * 1.1),
                    (this.sprite.length * ClientVars.toClientPixelSize(this.size) / 2),
                    (Math.atan2(transform.b, transform.a) * 180 / Math.PI) + this.relativePos.r - 90
                );
                return [
                    { x: bottomPoint.x, y: bottomPoint.y },
                    { x: topPoint.x, y: topPoint.y }
                ];
            }
            case WeaponType.SPEAR:
                return [
                    { x: box.x + this.originAtkLine[0].x, y: box.y + this.originAtkLine[0].y + (transform.f * 1.1) },
                    { x: box.x + this.originAtkLine[1].x, y: box.y + this.originAtkLine[1].y + (transform.f * 1.1) }
                ];
        }
    }

    retrieveAnglePoint(x, y, length, angle) {
        return {
            x: Math.round(x + length * Math.cos(angle * Math.PI / 180)),
            y: Math.round(y + length * Math.sin(angle * Math.PI / 180))
        };
    }

    update() {
        // if (GameVars.atkCanv) {
        if (this.isPerformingAction && !isProjectileWeapon(this.weaponType)) {
            let transform = new WebKitCSSMatrix(window.getComputedStyle(this.weaponCanv).transform);
            this.atkLine = this.getUpdatedWeaponAtkLine(this.parentDiv.getBoundingClientRect(), transform);

            // console.log("BEFORE", this.atkLine[1].y, ClientVars.reverseClientPixelSize(this.atkLine[1].y));

            // this.atkLine[0].x = ClientVars.reverseClientPixelSize(this.atkLine[0].x);
            // this.atkLine[0].y = ClientVars.reverseClientPixelSize(this.atkLine[0].y);

            // this.atkLine[1].x = ClientVars.reverseClientPixelSize(this.atkLine[1].x);
            // this.atkLine[1].y = ClientVars.reverseClientPixelSize(this.atkLine[1].y);

            // console.log("AFTER", this.atkLine[1].y, ClientVars.reverseClientPixelSize(this.atkLine[1].y));

            // just for debug
            // const ctx = GameVars.atkCanv.getContext("2d");
            // ctx.beginPath();
            // ctx.moveTo(newAtkLine[0].x, newAtkLine[0].y);
            // ctx.lineTo(newAtkLine[1].x, newAtkLine[1].y);
            // ctx.strokeStyle = 'red';
            // ctx.lineWidth = ClientVars.toClientPixelSize(1);
            // ctx.stroke();

            // instead save the atk line so the update can pick it and send it to the logic

            // if (this.isPlayer) {
            //     GameVars.currentRoom.enemies.forEach(enemy => lineCircleCollision(newAtkLine, enemy.collisionObj) && this.dealDmgToBlock(enemy));
            // } else {
            //     lineCircleCollision(newAtkLine, GameVars.player.collisionObj) && this.dealDmgToBlock(GameVars.player);
            // }
        }
        // }
    }

    action() {
        if (!this.isPerformingAction) {
            // GameVars.sound.atkSound();
            this.isPerformingAction = true;
            isProjectileWeapon(this.weaponType) && this.createProjectile();
            this.atkAnimation = this.getWeaponAnimation();
            this.atkAnimation.finished.then(() => {
                this.isPerformingAction = false;
                this.atkLine = {};
                // this.damagedObjs.clear();
            });
        }
    }

    createProjectile() {
        // const box = this.weaponCanv.getBoundingClientRect();
        // GameVars.currentRoom.projectiles.push(
        //     new Projectile(box.x + (box.width / 2), box.y + (box.height / 2), this.handDir, this.color, this.size, getProjectileType(this.weaponType), this.isPlayer)
        // );
    }

    draw() {
        drawSprite(this.weaponCanv.getContext("2d"), this.sprite, ClientVars.toClientPixelSize(this.size), null, null, { "wc": this.color });
    }

    destroy() {
        this.weaponDiv.remove();
    }
}