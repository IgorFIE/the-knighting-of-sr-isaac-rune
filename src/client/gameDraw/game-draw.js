import { getInputKey } from "../../enums/movement-type";
import { ClientVars } from "../client-variables";
import { PlayerDraw } from "./drawEntities/player-draw";
import { RoomDraw } from "./drawEntities/room-draw";
import { MovePad } from "./movepad";
import { WeaponPad } from "./weaponpad";

export class GameDraw {
    constructor(game, players, yourPlayerId) {
        // generate all draw
        this.gameRooms = this.GenerateGameRooms(game.gameBoard.roomBoard, game.gameLevel);
        this.hideRooms(game.gameBoard.roomsPos);
        this.gameRooms[game.currentRoomPos.y][game.currentRoomPos.x].show();

        this.players = {};

        // main player
        this.players[yourPlayerId] = new PlayerDraw(players[yourPlayerId], true);
        this.weaponPad = new WeaponPad(this.players[yourPlayerId]);
        this.movePad = new MovePad(this.players[yourPlayerId]);

        this.addKeyboardListenerEvents();
    }

    addKeyboardListenerEvents() {
        window.addEventListener('keydown', (e) => this.updateKeys(e.key, true));
        window.addEventListener('keyup', (e) => this.updateKeys(e.key, false));
    }

    updateKeys(key, isDown) {
        ClientVars.inputChanged = ClientVars.keys[key] !== isDown;
        ClientVars.keys[getInputKey(key)] = isDown;
        this.weaponPad?.update();
    }

    GenerateGameRooms(roomBoard, gameLevel) {
        const gameRooms = [];
        for (let y = 0; y < roomBoard.length; y++) {
            gameRooms.push([]);
            for (let x = 0; x < roomBoard[y].length; x++) {
                if (roomBoard[y][x]) {
                    gameRooms[y].push(new RoomDraw(roomBoard[y][x], gameLevel));
                } else {
                    gameRooms[y].push(null);
                }
            }
        }
        return gameRooms;
    }

    hideRooms(roomsPos) {
        roomsPos.forEach(roomPos => this.gameRooms[roomPos.y][roomPos.x]?.hide());
    }

    update(game) {
        Object.keys(game.players).forEach(id => {
            if (!this.players[id]) {
                this.players[id] = new PlayerDraw(game.players[id], false);
            }
            this.players[id].handleInputAnimations(game.players[id].keys);
        });
        this.movePad.update();
    }
}