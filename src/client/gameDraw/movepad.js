import { convertTextToPixelArt, drawPixelTextInCanvas } from "../../assets/text";
import { InputKey } from "../../enums/movement-type";
import { genSmallBox } from "../../utilities/box-generator-util";
import { createElem, setElemSize } from "../../utilities/element-util";
import { ClientVars } from "../client-variables";


export class MovePad {
    constructor(player) {
        this.playerColors = player.playerColors;
        this.movePadCanv = createElem(ClientVars.gameDiv, "canvas", null, null, null, null, null,
            (e) => {
                const canvBox = this.movePadCanv.getBoundingClientRect();
                const touch = e.type == "mousedown" ? e : e.changedTouches[0];

                const xAmount = ((touch.pageX - canvBox.x) - (this.movePadCanv.width / 2)) / (this.movePadCanv.width / 2);
                const yAmount = ((touch.pageY - canvBox.y) - (this.movePadCanv.height / 2)) / (this.movePadCanv.height / 2);

                const xFinalValue = (Math.abs(xAmount) >= 0.2 ? 1 : 0) * (xAmount < 0 ? -1 : 1);
                const yFinalValue = (Math.abs(yAmount) >= 0.2 ? 1 : 0) * (yAmount < 0 ? -1 : 1);

                ClientVars.inputChanged = ClientVars.keys[InputKey.UP] !== yFinalValue < 0 ||
                    ClientVars.keys[InputKey.DOWN] !== yFinalValue > 0 ||
                    ClientVars.keys[InputKey.LEFT] !== xFinalValue < 0 ||
                    ClientVars.keys[InputKey.RIGHT] !== xFinalValue > 0;

                ClientVars.keys[InputKey.UP] = yFinalValue < 0;
                ClientVars.keys[InputKey.DOWN] = yFinalValue > 0;
                ClientVars.keys[InputKey.LEFT] = xFinalValue < 0;
                ClientVars.keys[InputKey.RIGHT] = xFinalValue > 0;

                ClientVars.inputChanged && this.update();
            },
            (e) => {
                ClientVars.inputChanged = true;
                ClientVars.keys[InputKey.UP] = false;
                ClientVars.keys[InputKey.DOWN] = false;
                ClientVars.keys[InputKey.LEFT] = false;
                ClientVars.keys[InputKey.RIGHT] = false;
                this.update();
            }
        );
        this.update();
    }

    update() {
        setElemSize(this.movePadCanv, ClientVars.toClientPixelSize(64), ClientVars.toClientPixelSize(64));
        this.movePadCanv.style.translate = ClientVars.toClientPixelSize(12) + 'px ' +
            (ClientVars.gameH - this.movePadCanv.height - ClientVars.toClientPixelSize(12)) + 'px';

        const movepadCtx = this.movePadCanv.getContext("2d");
        movepadCtx.clearRect(0, 0, this.movePadCanv.width, this.movePadCanv.height);

        genSmallBox(movepadCtx, 13, 13, 6, 6, ClientVars.toClientPixelSize(2), this.playerColors.hl, "#100f0f66");

        genSmallBox(movepadCtx, 11, 1, 10, 10, ClientVars.toClientPixelSize(2), ClientVars.keys[InputKey.UP] ? "#ffffffaa" : "#00000066", ClientVars.keys[InputKey.UP] ? "#ffffff66" : "#100f0f66");
        drawPixelTextInCanvas(convertTextToPixelArt('^'), movepadCtx, ClientVars.toClientPixelSize(3), 11, 4, "#edeef7", 1);

        genSmallBox(movepadCtx, 1, 11, 10, 10, ClientVars.toClientPixelSize(2), ClientVars.keys[InputKey.LEFT] ? "#ffffffaa" : "#00000066", ClientVars.keys[InputKey.LEFT] ? "#ffffff66" : "#100f0f66");
        drawPixelTextInCanvas(convertTextToPixelArt('<'), movepadCtx, ClientVars.toClientPixelSize(3), 4, 11, "#edeef7", 1);

        genSmallBox(movepadCtx, 21, 11, 10, 10, ClientVars.toClientPixelSize(2), ClientVars.keys[InputKey.RIGHT] ? "#ffffffaa" : "#00000066", ClientVars.keys[InputKey.RIGHT] ? "#ffffff66" : "#100f0f66");
        drawPixelTextInCanvas(convertTextToPixelArt('>'), movepadCtx, ClientVars.toClientPixelSize(3), 18, 11, "#edeef7", 1);

        genSmallBox(movepadCtx, 11, 21, 10, 10, ClientVars.toClientPixelSize(2), ClientVars.keys[InputKey.DOWN] ? "#ffffffaa" : "#00000066", ClientVars.keys[InputKey.DOWN] ? "#ffffff66" : "#100f0f66");
        drawPixelTextInCanvas(convertTextToPixelArt('~'), movepadCtx, ClientVars.toClientPixelSize(3), 11, 18, "#edeef7", 1);
    }
}