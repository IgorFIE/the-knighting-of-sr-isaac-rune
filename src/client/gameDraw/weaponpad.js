import { convertTextToPixelArt, drawPixelTextInCanvas } from "../../assets/text";
import { InputKey } from "../../enums/movement-type";
import { WeaponType, getWeaponSprite } from "../../enums/weapon-type";
import { genSmallBox } from "../../utilities/box-generator-util";
import { createElem, setElemSize } from "../../utilities/element-util";
import { drawSprite } from "../../utilities/pixel-util";
import { ClientVars } from "../client-variables";


export class WeaponPad {
    constructor(player) {
        this.player = player;
        this.leftCanv = createElem(ClientVars.gameDiv, "canvas", null, null, null, null, null,
            (e) => {
                ClientVars.inputChanged = ClientVars.keys[InputKey.LEFT_ATK] !== true;
                ClientVars.keys[InputKey.LEFT_ATK] = true;
                ClientVars.inputChanged && this.update();
            },
            (e) => {
                ClientVars.keys[InputKey.LEFT_ATK] = false;
                this.update();
                ClientVars.inputChanged = true;
            });
        this.rightCanv = createElem(ClientVars.gameDiv, "canvas", null, null, null, null, null,
            (e) => {
                ClientVars.inputChanged = ClientVars.keys[InputKey.RIGHT_ATK] !== true;
                ClientVars.keys[InputKey.RIGHT_ATK] = true;
                ClientVars.inputChanged && this.update();
            },
            (e) => {
                ClientVars.keys[InputKey.RIGHT_ATK] = false;
                this.update();
                ClientVars.inputChanged = true;
            });
        this.update();
    }

    update() {
        setElemSize(this.leftCanv, ClientVars.toClientPixelSize(30), ClientVars.toClientPixelSize(30));
        setElemSize(this.rightCanv, ClientVars.toClientPixelSize(30), ClientVars.toClientPixelSize(30));

        this.leftCanv.style.translate = (ClientVars.gameW - this.leftCanv.width - ClientVars.toClientPixelSize(30 + 24)) + 'px ' + (ClientVars.gameH - this.leftCanv.height - ClientVars.toClientPixelSize(12)) + 'px';
        this.rightCanv.style.translate = (ClientVars.gameW - this.leftCanv.width - ClientVars.toClientPixelSize(12)) + 'px ' + (ClientVars.gameH - this.leftCanv.height - ClientVars.toClientPixelSize(12)) + 'px';

        this.drawIcon(this.leftCanv, "A", this.player.playerLeftWeapon.weaponType, ClientVars.keys[InputKey.LEFT_ATK]);
        this.drawIcon(this.rightCanv, "B", this.player.playerRightWeapon.weaponType, ClientVars.keys[InputKey.RIGHT_ATK]);
    }

    drawIcon(canvas, letter, weaponType, isTouch) {
        const ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        genSmallBox(ctx, 0, 0, 14, 14, ClientVars.toClientPixelSize(2), isTouch ? "#ffffffaa" : "#00000066", isTouch ? "#ffffff66" : "#100f0f66");
        this.drawWeapon(ctx, weaponType);
        genSmallBox(ctx, 9, 9, 5, 5, ClientVars.toClientPixelSize(2), this.player.playerColors.hl, "#100f0f66");
        drawPixelTextInCanvas(convertTextToPixelArt(letter), ctx, ClientVars.toClientPixelSize(1), 24, 24, "#edeef7", 1);
    }

    drawWeapon(ctx, weaponType) {
        let pixelSize, x, y;
        switch (weaponType) {
            case WeaponType.TROWING_AXE:
            case WeaponType.TROWING_KNIVE:
            case WeaponType.FIST:
                pixelSize = 6; x = 1; y = 1;
                break;
            case WeaponType.AXE:
            case WeaponType.SHIELD:
                pixelSize = 5; x = 1; y = 1;
                break;
            case WeaponType.SWORD:
                pixelSize = 5; x = 1; y = -2;
                break;
            case WeaponType.GREATSWORD:
                pixelSize = 4; x = 0; y = -2;
                break;
            case WeaponType.SPEAR:
                pixelSize = 5; x = 2; y = -8;
                break;
            case WeaponType.HAMMER:
            case WeaponType.MORNING_STAR:
                pixelSize = 5; x = 1; y = 2;
                break;
            case WeaponType.HALBERD:
                pixelSize = 5; x = 1; y = -1;
                break;
            case WeaponType.CROSSBOW:
                pixelSize = 4; x = -1; y = 0;
                break;
        }
        drawSprite(ctx, getWeaponSprite(weaponType), ClientVars.toClientPixelSize(pixelSize), x, y, { "wc": this.player.playerColors.hd });
    }
}