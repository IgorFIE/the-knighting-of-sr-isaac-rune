import { KnightSprite } from "../../assets/sprites";
import { Hand } from "../../enums/hand-type";
import { getWeaponSprite } from "../../enums/weapon-type";
import { createFloorBlock, createWallBlock } from "../../utilities/block-util";
import { genSmallBox } from "../../utilities/box-generator-util";
import { createElem, setElemSize } from "../../utilities/element-util";
import { randomNumbOnRange } from "../../utilities/general-util";
import { drawSprite } from "../../utilities/pixel-util";
import { ClientVars } from "../client-variables";
import { MenuBtn } from "./menu-btn";

export class MainMenu {

    constructor(parentElem, playerColors) {
        this.playerColors = playerColors;
        this.isDisplaying = false;
        this.mainMenuDiv = createElem(parentElem, "div");
        this.mainMenuCanvas = createElem(this.mainMenuDiv, "canvas", "main-menu");

        setElemSize(this.mainMenuCanvas, ClientVars.gameW, ClientVars.gameH);

        this.leftBtn = new MenuBtn(this.mainMenuDiv);
        this.rightBtn = new MenuBtn(this.mainMenuDiv);

        this.reInit();
    }

    reInit() {
        this.leftBtn.reInit(randomNumbOnRange(1, 6), Hand.LEFT)
        this.rightBtn.reInit(randomNumbOnRange(1, 6), Hand.RIGHT)

        this.draw();
        this.show();
    }

    draw() {
        const ctx = this.mainMenuCanvas.getContext("2d");
        for (let y = 0; y < ClientVars.gameH; y += ClientVars.toClientPixelSize(16)) {
            for (let x = 0; x < ClientVars.gameW; x += ClientVars.toClientPixelSize(16)) {
                if (y < ClientVars.gameH / 2) {
                    createWallBlock(ctx, x, y, 0);
                } else {
                    createFloorBlock(ctx, x, y, 0);
                }
            }
        }
        const wKnightCenter = ((ClientVars.gameW % 2 === 0 ? ClientVars.gameW : ClientVars.gameW + 1) / ClientVars.toClientPixelSize(10)) / 2;
        const hKnightCenter = ((ClientVars.gameH % 2 === 0 ? ClientVars.gameH : ClientVars.gameH + 1) / ClientVars.toClientPixelSize(10)) / 2;

        genSmallBox(ctx, wKnightCenter - 3.5, hKnightCenter + 1, 6, 5, ClientVars.toClientPixelSize(10), "#00000033", "#00000033");
        drawSprite(ctx, KnightSprite, ClientVars.toClientPixelSize(10), wKnightCenter - 1.5, hKnightCenter - 3.5, this.playerColors);

        const leftSprite = getWeaponSprite(this.leftBtn.weaponType);
        const rightSprite = getWeaponSprite(this.rightBtn.weaponType);

        drawSprite(ctx, leftSprite, ClientVars.toClientPixelSize(10), wKnightCenter - 4 - (leftSprite[0].length / 2), hKnightCenter - 1.5, { "wc": this.playerColors.hd });
        drawSprite(ctx, rightSprite, ClientVars.toClientPixelSize(10), wKnightCenter + 4 - (rightSprite[0].length / 2), hKnightCenter - 1.5, { "wc": this.playerColors.hd });

        // genSmallBox(ctx, -1, -1, Math.floor(ClientVars.gameW / toPixelSize(2)) + 2, 32, toPixelSize(2), "#060606", "#060606");
        // drawPixelTextInCanvas(convertTextToPixelArt("the knighting of"), ctx, toPixelSize(3), Math.round(ClientVars.gameW / 2 / toPixelSize(3)), 11, "#edeef7", 1);
        // drawPixelTextInCanvas(convertTextToPixelArt("sr isaac"), ctx, toPixelSize(2), Math.round(ClientVars.gameW / 2 / toPixelSize(2)), 25, "#edeef7", 1);

        // genSmallBox(ctx, -1, Math.floor(ClientVars.gameH / toPixelSize(2)) - 16, Math.floor(ClientVars.gameW / toPixelSize(2)) + 2, 17, toPixelSize(2), "#060606", "#060606");
        // drawPixelTextInCanvas(convertTextToPixelArt("each weapon has a different atk pattern"), ctx, toPixelSize(1), Math.round(ClientVars.gameW / 2 / toPixelSize(1)), Math.round(ClientVars.gameH / toPixelSize(1)) - 24, "#edeef7", 1);
        // drawPixelTextInCanvas(convertTextToPixelArt("js13kgames 2023 - igor estevao"), ctx, toPixelSize(1), Math.round(ClientVars.gameW / 2 / toPixelSize(1)), Math.round(ClientVars.gameH / toPixelSize(1)) - 8, "#edeef7", 1);
    }

    show() {
        this.mainMenuDiv.classList.remove("hidden");
        this.isDisplaying = true;
    }

    hide() {
        this.mainMenuDiv.classList.add("hidden");
        this.isDisplaying = false;
    }
}