import { convertTextToPixelArt, drawPixelTextInCanvas } from "../../assets/text";
import { Hand } from "../../enums/hand-type";
import { genSmallBox } from "../../utilities/box-generator-util";
import { createElem, setElemSize } from "../../utilities/element-util";
import { ClientVars } from "../client-variables";

export class MenuBtn {

    constructor(parentElem) {
        this.btnCanvas = createElem(parentElem, "canvas", null, null, null, null, null, () => this.setWeapon());
        setElemSize(this.btnCanvas, ClientVars.toClientPixelSize(66), ClientVars.toClientPixelSize(30));
    }

    setWeapon() {
        Rune.actions.selectWeapon({ weapon: this.weaponType, hand: this.hand });
    }

    reInit(weaponType, hand) {
        this.weaponType = weaponType;
        this.hand = hand;

        this.draw();
    }

    draw() {
        const ctx = this.btnCanvas.getContext("2d");
        genSmallBox(ctx, 0, 0, 32, 14, ClientVars.toClientPixelSize(2), "#060606", "#060606");

        const handPos = this.btnCanvas.width * (this.hand === Hand.LEFT ? -1 : 1);
        const centerXPos = (ClientVars.gameW / 2) - (this.btnCanvas.width / 2);
        const yPos = ClientVars.gameH - this.btnCanvas.height - ClientVars.toClientPixelSize(6);
        this.btnCanvas.style.translate = (centerXPos + handPos) + 'px ' + yPos + 'px';

        drawPixelTextInCanvas(convertTextToPixelArt("start game"), ctx, ClientVars.toClientPixelSize(1), 35, 10, "#edeef7", 1);

        const weaponText = "with " + (this.hand === Hand.LEFT ? "l" : "r") + " weapon";
        drawPixelTextInCanvas(convertTextToPixelArt(weaponText), ctx, ClientVars.toClientPixelSize(1), 33, 20, "#edeef7", 1);
    }
}