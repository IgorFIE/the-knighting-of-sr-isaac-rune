export const GameState = {
    MAIN_MENU: "MAIN_MENU",
    INIT_GAME: "INIT_GAME",
    PLAYING: "PLAYING"
}