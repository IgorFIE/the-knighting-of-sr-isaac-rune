export const InputKey = {
    UP: "UP",
    DOWN: "DOWN",
    LEFT: "LEFT",
    RIGHT: "RIGHT",

    LEFT_ATK: "LEFT_ATK",
    RIGHT_ATK: "RIGHT_ATK",
}

export const getInputKey = (key) => {
    switch (key) {
        case "ArrowUp":
        case "w":
        case "W":
            return InputKey.UP;

        case "ArrowDown":
        case "s":
        case "S":
            return InputKey.DOWN;

        case "ArrowLeft":
        case "a":
        case "A":
            return InputKey.LEFT;

        case "ArrowRight":
        case "d":
        case "D":
            return InputKey.RIGHT;

        case "v":
        case "V":
            return InputKey.LEFT_ATK;

        case "b":
        case "B":
            return InputKey.RIGHT_ATK;
    }
}