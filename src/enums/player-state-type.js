export const PlayerState = {
    PENDING: "PENDING",
    READY: "READY",
    ALIVE: "ALIVE",
    DEAD: "DEAD"
}