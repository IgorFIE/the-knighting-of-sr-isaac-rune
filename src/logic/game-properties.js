import { isSpecialRoom } from "../enums/room-type";
import { randomNumb } from "../utilities/general-util";
import { GameBoard } from "./logicEntities/game-board";


export class GameProperties {
    constructor() {
        this.isGameOver = false;
        this.gameBoardSize = 5;
        this.gameLevel = 1;

        this.gameBoard = new GameBoard(this.gameBoardSize);
        this.gameBoard.init();

        const mainRoom = this.getStartRoom(this.gameBoard.roomBoard[this.gameBoard.roomBoard.length / 2][this.gameBoard.roomBoard[0].length / 2]);
        this.mainRoomPos = mainRoom.pos;

        this.currentRoomPos = this.mainRoomPos;
        this.nextRoomPos;

        this.visitedRoomsPos = {};
    }

    getStartRoom(room) {
        if (!isSpecialRoom(room)) return room;
        return this.getStartRoom(this.gameBoard.roomsPos[randomNumb(this.gameBoard.roomsPos.length)]);
    }
}