export const playerSpeed = 4;

export const playerStartHeartCount = 3;
export const heartLifeVal = 6;

export const genericBlockSize = 16;
export const horizontalRoomBlocks = 25;
export const verticalRoomBlocks = 15;