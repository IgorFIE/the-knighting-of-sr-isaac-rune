import { GameState } from "../enums/game-state-type";
import { Hand } from "../enums/hand-type";
import { PlayerState } from "../enums/player-state-type";
import { WeaponType } from "../enums/weapon-type";
import { CircleObject } from "./collision-objects/circle-object";
import { genericBlockSize, heartLifeVal, horizontalRoomBlocks, playerSpeed, playerStartHeartCount, verticalRoomBlocks } from "./game-variables";
import { updatePlayerPosition } from "./logicHandler/playerHandler";
import { setupGameBoard } from "./logicHandler/setup-game";

Rune.initLogic({
  minPlayers: 1,
  maxPlayers: 4,
  landscape: true,
  setup: (allPlayerIds) => ({
    seed: Math.random(),
    gameState: GameState.MAIN_MENU,
    gameProperties: null,
    players: Object.fromEntries(allPlayerIds.map((playerId, index) => [playerId, generateNewPlayer(playerId, index, allPlayerIds.length)])),
  }),
  inputDelay: 25, // 25 default
  actions: {
    selectWeapon(playerWeapon, { game, playerId }) {
      if (game.gameState == GameState.MAIN_MENU) {

        if (playerWeapon.hand === Hand.LEFT) {
          game.players[playerId].leftHand = playerWeapon;
        } else {
          game.players[playerId].rightHand = playerWeapon;
        }
        game.players[playerId].playerState = PlayerState.READY;

        const players = Object.values(game.players);
        const areAlPlayersReady = players.filter(playerInfo => playerInfo.playerState == PlayerState.READY).length == players.length;

        if (areAlPlayersReady) {
          setupGameBoard(game.seed);
          game.gameState = GameState.INIT_GAME;
        }
      }
    },
    drawCompleted(_, { game }) {
      if (game.gameState == GameState.INIT_GAME) {
        game.gameState = GameState.PLAYING;
      }
    },
    updatePlayer(updateData, { game, playerId }) {
      game.players[playerId].keys = updateData.keys;
      game.players[playerId].atks = updateData.atks;
    }
  },
  updatesPerSecond: 30,
  update: ({ game }) => {
    if (game.gameState == GameState.PLAYING) {
      Object.keys(game.players).forEach(playerId => {
        game.players[playerId] = updatePlayerPosition(game.players[playerId]);
      });
    }
  },
  events: {
    playerJoined: (playerId, { game }) => {
      game.players[playerId] = generateNewPlayer(playerId, getColorIdex(game.players));
    },
    playerLeft: (playerId, { game }) => {
      delete game.players[playerId];
    },
  },
});

function generateNewPlayer(playerId, index, totalPlayers) {
  return {
    playerId,
    colors: playerColors[index],
    playerState: PlayerState.ALIVE,
    hasKey: false,
    keys: {},
    atks: {},
    playerSpeed: playerSpeed,
    collisionObj: getInitialPos(index, totalPlayers),
    life: heartLifeVal * playerStartHeartCount,
    leftHand: { weapon: WeaponType.FIST, hand: Hand.LEFT },
    rightHand: { weapon: WeaponType.FIST, hand: Hand.RIGHT }
  }
}

function getColorIdex(players) {
  let usedColors = Object.values(players).map((p) => p.colors);
  let indexToUse = 0;
  playerColors.find((color, index) => {
    if (!usedColors.includes(color)) {
      indexToUse = index;
    }
  });
  return indexToUse;
}

export const playerColors = [
  { "hd": "#cd9722", "hl": "#ffff57", "cm": "#9e6800" },
  { "hd": "#00bcd4", "hl": "#9bf2fa", "cm": "#007792" },
  { "hd": "#41ae3d", "hl": "#52ff4d", "cm": "#2f702c" },
  { "hd": "#a80000", "hl": "#ff392e", "cm": "#811f14" },
]

function getInitialPos(index, totalPlayers) {
  const yPos = totalPlayers > 2 ? index < 2 ? -1 : 1 : 0;
  const xPos = totalPlayers > 1 ? index % 2 == 0 ? -1 : 1 : 0;
  const halfWidth = (horizontalRoomBlocks * genericBlockSize) / 2;
  const halfHeight = (verticalRoomBlocks * genericBlockSize) / 2;

  return new CircleObject(
    halfWidth + (halfWidth / 3 * xPos),
    halfHeight + (halfHeight / 4 * yPos),
    4)
}
