import { Position } from "../../entities/position";
import { DoorType } from "../../enums/door-type";
import { SquareObject } from "../collision-objects/square-object";

export class Block {
    constructor(roomPos, blockRoomX, blockRoomY, blockType) {
        this.roomPos = roomPos;
        this.pos = new Position(blockRoomX, blockRoomY);
        this.collisionObj = new SquareObject(blockRoomX * 16, blockRoomY * 16, 16, 16);
        this.blockType = blockType;
        this.doorType = DoorType.NORMAL;
    }
}