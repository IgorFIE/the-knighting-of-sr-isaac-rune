import { DoorType } from "../../enums/door-type";
import { RoomType, isSpecialRoom } from "../../enums/room-type";
import { randomNumb } from "../../utilities/general-util";
import { Room } from "./room";

export class GameBoard {
    constructor(size) {
        this.size = size;
        this.roomBoard = [];
        this.roomsPos = [];
    }

    init() {
        this.initBoardArray();
        this.createBoardRooms();
        this.createPaths();
    }

    initBoardArray() {
        for (let y = 0; y < this.size * 2; y++) {
            this.roomBoard.push([]);
            for (let x = 0; x < this.size * 2; x++) {
                this.roomBoard[y].push(null);
            }
        }
    }

    createBoardRooms() {
        let x = Math.floor(this.roomBoard.length / 2);
        let y = Math.floor(this.roomBoard.length / 2);

        while (this.roomsPos.length != this.size) {
            if (this.roomBoard[y][x] == null) {
                this.roomBoard[y][x] = new Room(x, y);
                this.roomsPos.push(this.roomBoard[y][x].pos);
            }

            switch (randomNumb(4)) {
                case 0:
                    y - 1 >= 0 && y--;
                    break;
                case 1:
                    y + 1 < this.roomBoard.length && y++;
                    break;
                case 2:
                    x - 1 >= 0 && x--;
                    break;
                case 3:
                    x + 1 < this.roomBoard.length && x++;
                    break;
            }
        }
        this.createSpecialRoom(RoomType.KEY);
        this.createSpecialRoom(RoomType.TREASURE);
        this.createSpecialRoom(RoomType.BOSS);
    }

    createSpecialRoom(roomType) {
        let x = randomNumb(this.roomBoard.length);
        let y = randomNumb(this.roomBoard.length);
        while (!this.canPlaceSpecialRoom(x, y)) {
            x = randomNumb(this.roomBoard.length);
            y = randomNumb(this.roomBoard.length);
        }
        this.roomBoard[y][x] = new Room(x, y);
        this.roomBoard[y][x].setSpecialRoomType(roomType);
        this.roomsPos.push(this.roomBoard[y][x].pos);
    }

    canPlaceSpecialRoom(x, y) {
        if (this.roomBoard[y][x] == null) {
            let countSides = 0;
            y - 1 >= 0 && this.roomBoard[y - 1][x] != null && countSides++;
            y + 1 < this.roomBoard.length && this.roomBoard[y + 1][x] != null && countSides++;
            x - 1 >= 0 && this.roomBoard[y][x - 1] != null && countSides++;
            x + 1 < this.roomBoard.length && this.roomBoard[y][x + 1] != null && countSides++;
            return (countSides == 1 || countSides == 2) && !this.containsSpecialRoomArround(x, y);
        }
        return false;
    }

    containsSpecialRoomArround(startX, startY) {
        for (let y = -1; y <= 1; y++) {
            for (let x = -1; x <= 1; x++) {
                if (startX + x >= 0 && startX + x < this.roomBoard.length &&
                    startY + y >= 0 && startY + y < this.roomBoard.length &&
                    this.roomBoard[startY + y][startX + x] != null &&
                    isSpecialRoom(this.roomBoard[startY + y][startX + x].roomType)) {
                    return true;
                }
            }
        }
        return false;
    }

    createPaths() {
        let w, h, doorType, room;
        this.roomsPos.forEach((roomPos) => {
            room = this.roomBoard[roomPos.y][roomPos.x];
            w = room.backBlocks[0].length;
            h = room.backBlocks.length;
            if (room.pos.y - 1 >= 0 && this.roomBoard[room.pos.y - 1][room.pos.x]) {
                doorType = this.getDoorBasedOnRoomType(isSpecialRoom(this.roomBoard[room.pos.y][room.pos.x].roomType) ? this.roomBoard[room.pos.y][room.pos.x].roomType : this.roomBoard[room.pos.y - 1][room.pos.x].roomType);
                room.setDoor((w / 2) - 3, (w / 2) + 1, 0, 0, 0, -1, doorType);
                room.setDoor((w / 2) - 2, (w / 2), 1, 1, 0, -1, doorType);
            }
            if (room.pos.y + 1 < this.roomBoard.length && this.roomBoard[room.pos.y + 1][room.pos.x]) {
                doorType = this.getDoorBasedOnRoomType(isSpecialRoom(this.roomBoard[room.pos.y][room.pos.x].roomType) ? this.roomBoard[room.pos.y][room.pos.x].roomType : this.roomBoard[room.pos.y + 1][room.pos.x].roomType);
                room.setDoor((w / 2) - 2, (w / 2), h - 2, h - 2, 0, 1, doorType);
                room.setDoor((w / 2) - 3, (w / 2) + 1, h - 1, h - 1, 0, 1, doorType);
            }
            if (room.pos.x - 1 >= 0 && this.roomBoard[room.pos.y][room.pos.x - 1]) {
                doorType = this.getDoorBasedOnRoomType(isSpecialRoom(this.roomBoard[room.pos.y][room.pos.x].roomType) ? this.roomBoard[room.pos.y][room.pos.x].roomType : this.roomBoard[room.pos.y][room.pos.x - 1].roomType);
                room.setDoor(0, 0, (h / 2) - 3, (h / 2) + 1, -1, 0, doorType);
                room.setDoor(1, 1, (h / 2) - 2, (h / 2), -1, 0, doorType);
            }
            if (room.pos.x + 1 < this.roomBoard[0].length && this.roomBoard[room.pos.y][room.pos.x + 1]) {
                doorType = this.getDoorBasedOnRoomType(isSpecialRoom(this.roomBoard[room.pos.y][room.pos.x].roomType) ? this.roomBoard[room.pos.y][room.pos.x].roomType : this.roomBoard[room.pos.y][room.pos.x + 1].roomType);
                room.setDoor(w - 2, w - 2, (h / 2) - 2, (h / 2), 1, 0, doorType);
                room.setDoor(w - 1, w - 1, (h / 2) - 3, (h / 2) + 1, 1, 0, doorType);
            }
        });
    }

    getDoorBasedOnRoomType(roomType) {
        switch (roomType) {
            case RoomType.TREASURE:
                return DoorType.TREASURE;
            case RoomType.BOSS:
                return DoorType.BOSS;
        }
        return DoorType.NORMAL;
    }
}