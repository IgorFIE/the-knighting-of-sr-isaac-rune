import { Position } from "../../entities/position";
import { BlockType } from "../../enums/block-type";
import { RoomType } from "../../enums/room-type";
import { horizontalRoomBlocks, verticalRoomBlocks } from "../game-variables";
import { Block } from "./block";

export class Room {
    constructor(roomX, roomY) {
        this.pos = new Position(roomX, roomY);
        this.roomType = RoomType.EMPTY;

        this.backBlocks = [];
        this.floors = [];
        this.walls = [];

        this.doors = [];
        this.doorTriggers = [];

        this.items = [];
        this.enemies = [];
        this.projectiles = [];

        this.initRoomBlocks();
        // this.createSpikesAndStones();
        // this.populateRandomEnemies();
    }

    initRoomBlocks() {
        let obj, blockType;
        for (let y = 0; y < verticalRoomBlocks; y++) {
            this.backBlocks.push([]);
            for (let x = 0; x < horizontalRoomBlocks; x++) {
                blockType = (y <= 1 || x <= 1 || y >= verticalRoomBlocks - 2 || x >= horizontalRoomBlocks - 2) ? BlockType.WALL : BlockType.FLOOR;
                obj = new Block(this.pos, x, y, blockType);
                this.backBlocks[y].push(obj);
                blockType === BlockType.WALL ? this.walls.push(obj.pos) : this.floors.push(obj.pos);
            }
        }
    }

    // createSpikesAndStones() {
    //     this.spikesBlocks = [];
    //     this.stonesBlocks = [];
    //     // this.stonesBlocks.push(new Stone(this, 3, 3)); //FOR DEBUG
    //     for (let y = 0; y < GameVars.roomHeight; y++) {
    //         for (let x = 0; x < GameVars.roomWidth; x++) {
    //             if ((y > 3 && y < GameVars.roomHeight - 4 && x > 3 && x < GameVars.roomWidth - 4) &&
    //                 !(y > 6 && y < GameVars.roomHeight - 7 && x > 6 && x < GameVars.roomWidth - 7) &&
    //                 !(y > (GameVars.roomHeight / 2) - 3 && y < (GameVars.roomHeight / 2) + 3 && x > (GameVars.roomWidth / 2) - 3 && x < (GameVars.roomWidth / 2) + 3)) {
    //                 let random = randomNumb(100);
    //                 random >= 5 && random < 15 && this.stonesBlocks.push(new Stone(this, x, y));
    //                 random < 5 && this.spikesBlocks.push(new Spikes(this, x, y));
    //             }
    //         }
    //     }
    // }

    // populateRandomEnemies() {
    //     let count = randomNumbOnRange(GameVars.gameLevel, GameVars.gameLevel + 1);
    //     count = count > 10 ? 10 : count;
    //     while (this.enemies.length !== count) {
    //         let newEnemy = new Enemy(this,
    //             randomNumbOnRange(GameVars.gameW / 3, (GameVars.gameW / 3) * 2),
    //             randomNumbOnRange(GameVars.gameH / 3, (GameVars.gameH / 3) * 2),
    //             EnemyType.BASIC);
    //         if (!this.enemies.find(enemy => circleToCircleCollision(newEnemy.collisionObj, enemy.collisionObj)) &&
    //             !this.stonesBlocks.find(stone => rectCircleCollision(newEnemy.collisionObj, stone.collisionObj)) &&
    //             !this.spikesBlocks.find(spike => rectCircleCollision(newEnemy.collisionObj, spike.collisionObj))) {
    //             this.enemies.push(newEnemy);
    //         } else {
    //             newEnemy.div.remove();
    //         }
    //     }
    // }

    setSpecialRoomType(roomType) {
        this.roomType = roomType;
        // switch (this.roomType) {
        //     case RoomType.KEY:
        //         this.items.push(new Item(GameVars.gameW / 2, GameVars.gameH / 2, ItemType.KEY, null, this));
        //         break;
        //     case RoomType.TREASURE:
        //         this.cleanEnemies();
        //         this.items.push(new Item(
        //             (GameVars.gameW / 2) + toPixelSize(randomNumbOnRange(-32, 32)),
        //             (GameVars.gameH / 2) + toPixelSize(randomNumbOnRange(-32, 32)),
        //             ItemType.HEART, null, this));
        //         const maxValue = GameVars.gameLevel + 6 >= Object.keys(WeaponType).length ? Object.keys(WeaponType).length - 1 : GameVars.gameLevel + 6;
        //         this.items.push(new Item(GameVars.gameW / 2, GameVars.gameH / 2, ItemType.WEAPON, randomNumbOnRange(maxValue - 3, maxValue), this));
        //         break;
        //     case RoomType.BOSS:
        //         this.cleanEnemies();
        //         this.items.push(new Bonfire((GameVars.gameW / 2), (GameVars.gameH / 2), this));
        //         this.enemies.push(new Enemy(this, GameVars.gameW / 2, GameVars.gameH / 2, EnemyType.BOSS));
        //         break;
        // }
    }

    cleanEnemies() {
        while (this.enemies.length > 0) {
            this.enemies[0].destroy();
        }
    }

    setDoor(startX, finishX, startY, finishY, xDir, yDir, doorType) {
        let block;
        for (let y = Math.round(startY); y <= Math.round(finishY); y++) {
            for (let x = Math.round(startX); x <= Math.round(finishX); x++) {
                block = this.backBlocks[y][x];
                this.walls.splice(this.walls.indexOf(block.pos), 1);

                block.blockType = BlockType.DOOR_CLOSE;
                block.doorType = doorType;
                this.doors.push(block.pos);

                // this.backBlocks[y][x] = new DoorTrigger(this, block.blockRoomX, block.blockRoomY, BlockType.FLOOR, xDir, yDir);
                this.floors.push(this.backBlocks[y][x].pos);

                ((xDir === -1 && x === 0) || (xDir === 1 && x === this.backBlocks[0].length - 1) ||
                    (yDir === -1 && y === 0) || (yDir === 1 && y === this.backBlocks.length - 1)) && this.doorTriggers.push(this.backBlocks[y][x]);
            }
        }
    }
}