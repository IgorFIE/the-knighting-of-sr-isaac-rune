import { ClientVars } from "../../client/client-variables";
import { InputKey } from "../../enums/movement-type";
import { hasCollision } from "../../utilities/collision-utilities";
import { CircleObject } from "../collision-objects/circle-object";

export const updatePlayerPosition = (playerInfo) => {
    const room = ClientVars.gameProperties.gameBoard.roomBoard[ClientVars.gameProperties.currentRoomPos.y][ClientVars.gameProperties.currentRoomPos.x];
    let newRectX = playerInfo.collisionObj.x;
    let newRectY = playerInfo.collisionObj.y;

    const movKeys = Object.keys(playerInfo.keys).filter((key) => (
        key === InputKey.UP || key === InputKey.DOWN || key === InputKey.LEFT || key === InputKey.RIGHT
    ) && playerInfo.keys[key]);

    const distance = movKeys.length > 1 ? playerInfo.playerSpeed / 1.4142 : playerInfo.playerSpeed;
    if (playerInfo.keys[InputKey.RIGHT]) { newRectX += distance; }
    if (playerInfo.keys[InputKey.LEFT]) { newRectX -= distance; }
    if (playerInfo.keys[InputKey.UP]) { newRectY -= distance; }
    if (playerInfo.keys[InputKey.DOWN]) { newRectY += distance; }

    playerInfo.collisionObj = updateMovement(playerInfo.collisionObj, playerInfo.collisionObj.x, newRectY, room);
    playerInfo.collisionObj = updateMovement(playerInfo.collisionObj, newRectX, playerInfo.collisionObj.y, room);
    return playerInfo;
}

export const updateMovement = (oldPos, newX, newY, room, ignoreCollisions) => {
    const newCircle = new CircleObject(newX, newY, oldPos.r);
    if (ignoreCollisions || !hasCollision(oldPos, newCircle, room)) {
        return newCircle;
    }
    return oldPos;
}