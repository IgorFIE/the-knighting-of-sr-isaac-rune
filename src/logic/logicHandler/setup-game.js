import { ClientVars } from "../../client/client-variables";
import { SeedVars, initRandomSeed } from "../../utilities/general-util";
import { GameProperties } from "../game-properties";

export const setupGameBoard = (seed) => {
    if (!SeedVars.seedRandom) initRandomSeed(seed);
    SeedVars.useSeed = true;
    ClientVars.gameProperties = new GameProperties();
}