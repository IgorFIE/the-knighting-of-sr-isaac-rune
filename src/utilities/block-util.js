import { ClientVars } from "../client/client-variables";
import { DoorType } from "../enums/door-type";
import { generateBox } from "./box-generator-util";
import { randomNumb } from "./general-util";

export const getSpecialDoorColors = (dooType) => {
    return dooType === DoorType.TREASURE ? { "lt": "#ffff57", "md": "#cd9722", "dk": "#9e6800" } :
        dooType === DoorType.BOSS ? { "lt": "#703a33", "md": "#641f14", "dk": "#431313" } :
            { "lt": "#865433", "md": "#843d0d", "dk": "#2f1519" };
}

export const getWallBlockColors = (gameLevel) => {
    return gameLevel < 3 ? { "lt": "#999a9e", "md": "#686b7a", "dk": "#3e3846" } :
        gameLevel < 5 ? { "lt": "#703a33", "md": "#38252e", "dk": "#1b1116" } :
            { "lt": "#431313", "md": "#2f1519", "dk": "#100f0f" };
};

export const createWallBlock = (ctx, x, y, gameLevel) => {
    const blockColors = getWallBlockColors(gameLevel);
    ctx.fillStyle = blockColors.md;
    ctx.fillRect(x, y, ClientVars.toClientPixelSize(16), ClientVars.toClientPixelSize(16));

    ctx.fillStyle = blockColors.dk;
    ctx.fillRect(x, y + ClientVars.toClientPixelSize(14), ClientVars.toClientPixelSize(16), ClientVars.toClientPixelSize(2));
    ctx.fillRect(x, y, ClientVars.toClientPixelSize(2), ClientVars.toClientPixelSize(16));

    ctx.fillStyle = blockColors.lt;
    ctx.fillRect(x, y, ClientVars.toClientPixelSize(16), ClientVars.toClientPixelSize(2));
    ctx.fillRect(x + ClientVars.toClientPixelSize(14), y, ClientVars.toClientPixelSize(2), ClientVars.toClientPixelSize(16));
};

export const getFloorBlockColors = (gameLevel) => {
    return gameLevel < 3 ? { "lt": "#52804d", "md": "#41663d" } :
        gameLevel < 5 ? { "lt": "#41663d", "md": "#2f492c" } :
            { "lt": "#703a33", "md": "#38252e" };
};

export const createFloorBlock = (ctx, x, y, gameLevel) => {
    const floorColors = getFloorBlockColors(gameLevel);
    ctx.fillStyle = floorColors.md;
    ctx.fillRect(x, y, ClientVars.toClientPixelSize(16), ClientVars.toClientPixelSize(16));
    generateBox(ctx,
        convertToMapPixel(x), convertToMapPixel(y),
        convertToMapPixel(ClientVars.toClientPixelSize(14)), convertToMapPixel(ClientVars.toClientPixelSize(14)),
        ClientVars.toClientPixelSize(2), floorColors.lt, () => randomNumb(100) < 5);
};

export const convertToMapPixel = (value, amount = 2) => {
    return value / ClientVars.toClientPixelSize(amount);
};