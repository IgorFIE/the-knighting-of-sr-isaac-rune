import SeedRandom from "seed-random";

let useSeed;
let seedRandom;

const getRandomValue = () => {
    return !SeedVars.useSeed || !SeedVars.seedRandom ? Math.random() : SeedVars.seedRandom();
}

export const initRandomSeed = (seed) => {
    SeedVars.seedRandom = SeedRandom(seed.toString());
}

export const randomNumbOnRange = (min, max) => {
    return Math.floor(getRandomValue() * (max - min + 1)) + min;
}

export const randomNumb = (max) => {
    return Math.floor(getRandomValue() * max);
}

export const createId = () => {
    return getRandomValue().toString(16).slice(2);
}

export const SeedVars = {
    useSeed,
    seedRandom
}