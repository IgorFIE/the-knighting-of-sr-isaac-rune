export const drawSprite = (ctx, sprite, pixelSize = 1, startX = 0, startY = 0, colorIds = null) => {
    sprite.forEach((row, y) => row.forEach((val, x) => {
        if (val !== null) {
            ctx.fillStyle = colorIds ? colorIds[val] || val : val;
            ctx.fillRect(
                (startX * pixelSize) + (x * pixelSize),
                (startY * pixelSize) + (y * pixelSize),
                pixelSize,
                pixelSize);
        }
    }));
};